# ShowIndicator

[![CI Status](http://img.shields.io/travis/Banyong Jaisue/ShowIndicator.svg?style=flat)](https://travis-ci.org/Banyong Jaisue/ShowIndicator)
[![Version](https://img.shields.io/cocoapods/v/ShowIndicator.svg?style=flat)](http://cocoapods.org/pods/ShowIndicator)
[![License](https://img.shields.io/cocoapods/l/ShowIndicator.svg?style=flat)](http://cocoapods.org/pods/ShowIndicator)
[![Platform](https://img.shields.io/cocoapods/p/ShowIndicator.svg?style=flat)](http://cocoapods.org/pods/ShowIndicator)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ShowIndicator is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "ShowIndicator"
```

## Author

Banyong Jaisue, banyong.jaisue@allianz.com

## License

ShowIndicator is available under the MIT license. See the LICENSE file for more info.
